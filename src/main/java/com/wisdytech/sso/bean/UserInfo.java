package com.wisdytech.sso.bean;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apereo.cas.authentication.RememberMeUsernamePasswordCredential;

import javax.validation.constraints.Size;

public class UserInfo extends RememberMeUsernamePasswordCredential{
	
	@Size(min = 2, message = "require system")
    private String system;

	private String workNo;

	private String loginType;

	private String  ENABLED;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1160490876882710019L;

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public String getSystem() {
        return system;
    }

    public UserInfo setSystem(String system) {
        this.system = system;
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(this.system)
				.append(this.workNo)
				.append(this.loginType)
                .toHashCode();
    }

	public String getENABLED() {
		return ENABLED;
	}

	public UserInfo setENABLED(String eNABLED) {
		ENABLED = eNABLED;
		 return this;
	}
    
    
}
