package com.wisdytech.sso.handler;

import com.wisdytech.sso.bean.UserInfo;
import com.wisdytech.sso.conf.MD5Util;
import com.wisdytech.sso.exception.EnableUserException;
import com.wisdytech.sso.exception.PasswordErrorException;
import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.authentication.Credential;
import org.apereo.cas.authentication.HandlerResult;
import org.apereo.cas.authentication.PreventedException;
import org.apereo.cas.authentication.handler.support.AbstractPreAndPostProcessingAuthenticationHandler;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.services.ServicesManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.security.auth.login.FailedLoginException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Login extends AbstractPreAndPostProcessingAuthenticationHandler {
	
	private JdbcTemplate template;
	
	@Value("${jdbc.query.sql}")
	private String sql;

	@Value("${jdbc.query.sql-workno}")
	private String sqlWorkNo;

	public Login(String name, ServicesManager servicesManager, PrincipalFactory principalFactory, Integer order,JdbcTemplate template) {
		super(name, servicesManager, principalFactory, order);
		this.template =template;
	}

	@Override
	public boolean supports(Credential credential) {
		return credential instanceof UserInfo;
	}

	@Override
	protected HandlerResult doAuthentication(Credential credential)
			throws GeneralSecurityException, PreventedException {
		
		
		UserInfo sysCredential = (UserInfo) credential;
		//用户名
		String username =sysCredential.getUsername();
		//密码
		String password =sysCredential.getPassword();
		//登陆方式 1用户名 2密码
		String loginType = sysCredential.getLoginType();
		List<UserInfo> users = new ArrayList<>();
		//用户名密码登陆
		if (StringUtils.isNotBlank(loginType)&&"1".equals(loginType)) {
			users = template.query(sql, new Object[]{username}, new BeanPropertyRowMapper<>(UserInfo.class));
		} else if (StringUtils.isNotBlank(loginType)&&"2".equals(loginType)){
			//工卡登陆
			users = template.query(sqlWorkNo, new Object[]{username}, new BeanPropertyRowMapper<>(UserInfo.class));
		}

		if(null == users || users.size()==0) {
			//用户不存在
			 throw new FailedLoginException("用户不存在");
		}
		
		if(users.get(0).getENABLED().equals("0")) {
			//用户禁用
			throw new EnableUserException("用户禁用");
		}

		if (StringUtils.isNotBlank(loginType)&&"1".equals(loginType)) {
			password = MD5Util.getDigest(password);
			if(!password.equals(users.get(0).getPassword())) {
				throw new PasswordErrorException("密码错误");
			}
		}

		
		return createHandlerResult(credential, 
        		this.principalFactory.createPrincipal(users.get(0).getUsername(), Collections.emptyMap()), null);
		
		
	}

	
}
