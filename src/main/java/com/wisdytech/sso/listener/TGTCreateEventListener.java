package com.wisdytech.sso.listener;

import com.wisdytech.sso.service.TriggerLogoutService;
import com.wisdytech.sso.service.UsernameObtainServiceImpl;
import org.apereo.cas.support.events.ticket.CasTicketGrantingTicketCreatedEvent;
import org.apereo.cas.ticket.TicketGrantingTicket;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 识别TGT创建事件
 */
public class TGTCreateEventListener {

    private TriggerLogoutService logoutService;
    private UsernameObtainServiceImpl service;

    public TGTCreateEventListener(@NotNull TriggerLogoutService logoutService, @NotNull UsernameObtainServiceImpl service) {
        this.logoutService = logoutService;
        this.service = service;
    }

    @EventListener
    @Async
    public void onTgtCreateEvent(CasTicketGrantingTicketCreatedEvent event) {
        TicketGrantingTicket ticketGrantingTicket = event.getTicketGrantingTicket();
        String username = ticketGrantingTicket.getAuthentication().getPrincipal().getId();
        String tgt = ticketGrantingTicket.getId();
        String clientName = (String) ticketGrantingTicket.getAuthentication().getAttributes().get("clientName");
        //获取可以认证的id
        List<String> usernames = service.obtain(clientName, username);
        if (usernames != null&&usernames.size()>0) {
            //循环触发登出
            usernames.forEach(x -> logoutService.triggerLogout(x, tgt));
        }
    }
}
