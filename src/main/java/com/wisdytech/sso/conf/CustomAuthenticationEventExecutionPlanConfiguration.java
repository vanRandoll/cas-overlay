package com.wisdytech.sso.conf;

import org.apereo.cas.authentication.AuthenticationEventExecutionPlan;
import org.apereo.cas.authentication.AuthenticationEventExecutionPlanConfigurer;
import org.apereo.cas.authentication.AuthenticationHandler;
import org.apereo.cas.authentication.principal.DefaultPrincipalFactory;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.configuration.CasConfigurationProperties;
import org.apereo.cas.services.ServicesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.wisdytech.sso.handler.Login;

@Configuration("customAuthenticationEventExecutionPlanConfiguration")
@EnableConfigurationProperties(CasConfigurationProperties.class)
public class CustomAuthenticationEventExecutionPlanConfiguration  implements AuthenticationEventExecutionPlanConfigurer{
	
	
		@Autowired
	    @Qualifier("servicesManager")
	    private ServicesManager servicesManager;
	
	    @Autowired
	    @Qualifier("jdbcPrincipalFactory")
	    public PrincipalFactory jdbcPrincipalFactory;
	    
	    
	    @Value("${jdbc.query.driverClass}")
	    private String driverClassName;
	    @Value("${jdbc.query.url}")
	    private String url;
	    @Value("${jdbc.query.user}")
	    private String userName;
	    @Value("${jdbc.query.password}")
	    private String password;
	    

	    @Bean
	    public AuthenticationHandler myAuthenticationHandler() {
	    	
	    	// TODO Auto-generated method stub
	        DriverManagerDataSource d=new DriverManagerDataSource();
	        d.setDriverClassName(driverClassName);
	        d.setUrl(url);
	        d.setUsername(userName);
	        d.setPassword(password);
	        JdbcTemplate template=new JdbcTemplate();
	        template.setDataSource(d);
	    	
	    	
	        final Login handler = new Login(Login.class.getSimpleName(),
	        		servicesManager, 
	        		jdbcPrincipalFactory, 1
	        		,template);
	        return handler;
	    }

	    @Override
	    public void configureAuthenticationExecutionPlan(AuthenticationEventExecutionPlan plan) {
	        // TODO Auto-generated method stub
	        plan.registerAuthenticationHandler(myAuthenticationHandler());
	    }

}
