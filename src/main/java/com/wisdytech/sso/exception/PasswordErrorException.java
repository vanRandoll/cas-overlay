package com.wisdytech.sso.exception;

import javax.security.auth.login.AccountExpiredException;

/**
 * 密码错误异常
 * @author dlb
 *
 */
public class PasswordErrorException extends AccountExpiredException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3791349997010591472L;

	public PasswordErrorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PasswordErrorException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
	
	

}
