package com.wisdytech.sso.exception;

import javax.security.auth.login.AccountExpiredException;

/**
 * 用户禁用异常
 * @author dlb
 *
 */
public class EnableUserException extends AccountExpiredException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7870145289034941822L;

	public EnableUserException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnableUserException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	

}
